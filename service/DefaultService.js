'use strict';
const Schemas = require('./Schemas');

/**
 * Add
 * Add a Compra
 *
 * body AddCompraRequest Compra do Produto
 * returns AddCompraResponse
 **/
exports.add = function(body) {
  return new Promise(function(resolve, reject) {
    console.log(body);
    const { estoque, descricao, preco, imagem } = body;
    if (!estoque || !descricao || !preco) reject();
    const newOrder = new Schemas.Pedido({
      estoque: `${parseInt(estoque.split(" ")[0]) - 1} unidades`,
      descricao,
      preco,
      imagem
    });
    newOrder.save().then(resolve);
  });
}


/**
 * All
 * Get all produtos
 *
 * returns GetProdutosResponse
 **/
exports.all = function() {
  return new Promise(function(resolve, reject) {
    const products = Schemas.Product.find()
	  .then(resolve);
  });
}


/**
 *
 * id Integer The product ID
 * returns GetProdutoResponse
 **/
exports.produtosIdGET = function(id) {
  return new Promise(function(resolve, reject) {
    const products = Schemas.Product
      .find({ _id: id })
      .then(resolve);
  });
}

