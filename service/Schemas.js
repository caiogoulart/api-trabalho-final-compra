const mongoose = require('mongoose');

exports.Product = mongoose.model('Products', {
  estoque: String,
  descricao: String,
  preco: Number,
  imagem: String
});

exports.Pedido = mongoose.model('Pedido', {
  estoque: String,
  descricao: String,
  preco: Number,
  imagem: String
});
