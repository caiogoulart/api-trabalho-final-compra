'use strict';

var utils = require('../utils/writer.js');
var Default = require('../service/DefaultService');

module.exports.add = function add (req, res, next, body) {
  Default.add(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.all = function all (req, res, next) {
  Default.all()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.produtosIdGET = function produtosIdGET (req, res, next, id) {
  Default.produtosIdGET(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
